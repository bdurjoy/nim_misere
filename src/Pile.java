/*
 * Name: Durjoy Barua
 * Section: Intro to programming 300
 * Lab Instructor: Rejaul Chowdhury
 */

public class Pile {
	private int size;  // The current size of pile


	public Pile() {
		// Default constructor (Non-bonus version)
		size = 0;
	}
	public Pile(int size) {
		// Initial constructor (Bonus version)
		this.size = size;

	}
	public int getSize() {
		// get current size of pile
		return size;

	}
	public int remove(int subCh) {
		return size = size - subCh;

	}

}