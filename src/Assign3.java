/*
 * Name: Durjoy Barua
 * Section: Intro to programming 300
 * Lab Instructor: Rejaul Chowdhury
 */
public class Assign3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Nim nimGame = new Nim();

		System.out.println("Welcome to the NIM game\n" + 
				"We play by the mis�re rules");
		int i = 0;
		for(i = 0; !nimGame.done(); i++) {
			if(i%2 == 0) {
				nimGame.PlayerMove();
			}else {
				nimGame.computerMove();
			}
		}


		if(i%2 != 0) {
			System.out.println("\nSorry: you loose");
		}else {
			System.out.println("\nCongrats: you won");
		}
	}

}
