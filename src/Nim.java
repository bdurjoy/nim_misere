/*
 * Name: Durjoy Barua
 * Section: Intro to programming 300
 * Lab Instructor: Rejaul Chowdhury
 */

import java.util.*;

public class Nim {
	private Pile pileA;  // First pile
	private Pile pileB; // Second pile
	private Pile pileC; // Third pile
	private Random rnd;  // Random number generator
	private Scanner input;  // Scanner for all user input


	private char choice;


	public Nim() {
		// Default constructor, constructs the three piles
		rnd = new Random();
		input = new Scanner(System.in);


		pileA = new Pile(rnd.nextInt(11)+10);
		pileB = new Pile(rnd.nextInt(11)+10);
		pileC = new Pile(rnd.nextInt(11)+10);

	}

	public boolean PlayerMove() {
		// All the rules to handle user input
		printPiles();
		int pA = pileA.getSize();
		int pB = pileB.getSize();
		int pC = pileC.getSize();
		choice = 0;
		int numbInp = 0;


		System.out.print("\nSelect a Pile: ");
		choice = input.next().charAt(0);
		choice = Character.toLowerCase(choice);


		if(!Character.toString(choice).matches("^[a-cA-C]*$")) {
			System.out.println("Invalid pile letter. Select a, b or c");
			numbInp = 0;
			PlayerMove();
		}else {
			if(choice == 'a' && pA == 0) {
				System.out.print("Pile A is empty, pick another");
				PlayerMove();
			}else if(choice == 'b' && pB == 0) {
				System.out.print("Pile B is empty, pick another");
				PlayerMove();
			}else if(choice == 'c' && pC == 0) {
				System.out.print("Pile C is empty, pick another");
				PlayerMove();
			}
			
			if(numbInp == 0) {
				System.out.print("\nHow many do you want to remove? ");
				numbInp = input.nextInt();
			}

			if(choice == 'a' && numbInp <= pA && numbInp > 0) {
				pileA.remove(numbInp);
			}else if(choice == 'b' && numbInp <= pB && numbInp > 0) {
				pileB.remove(numbInp);
			}else if(choice == 'c' && numbInp <= pC && numbInp > 0) {
				pileC.remove(numbInp);
			}else {
				System.out.println("\nInvalid Input. ");
				PlayerMove();
			}
		}

		return true;

	}
	private void computerRandomMove() {
		// Computer move if done randomly, is computerMove() in non-bonus version
		int pA = pileA.getSize();
		int pB = pileB.getSize();
		int pC = pileC.getSize();

		if(((pB == 0 && pC == 1) || (pB == 1 && pC == 0)) && pA != 0) {
			choice = 'a';
		}else if(((pA == 0 && pC == 1) || (pA == 1 && pC == 0)) && pB != 0) {
			choice = 'b';
		}else if(((pA == 0 && pB == 1) || (pA == 1 && pB == 0)) && pC != 0) {
			choice = 'c';
		}
		else {
			int randC = rnd.nextInt(3)+1;

			if(randC == 1 && pA != 0) {
				choice = 'a';
			}else if(randC == 2 && pB != 0) {
				choice = 'b';
			}else if(randC == 3 && pC != 0) {
				choice = 'c';
			}else {
				computerRandomMove();
			}
		}


	}
	public void computerMove() {
		// All the rules to handle computer move

		computerRandomMove();

		int pA = pileA.getSize();
		int pB = pileB.getSize();
		int pC = pileC.getSize();
		int numbInp = 0;



		if(choice == 'a') {
			if(pA != 1 && pB == 0 && pC == 0) {
				numbInp = pA - 1;
			}else if((pB == 0 && pC == 1) || (pB == 1 && pC == 0)) {
				numbInp = pA;
			}else {
				numbInp = rnd.nextInt(pA)+1;
			}
			pileA.remove(numbInp);
		}else if(choice == 'b') {
			if(pA == 0 && pC == 0 && pB != 1) {
				numbInp = pB-1;
			}else if((pA == 0 && pC == 1) || (pA == 1 && pC == 0)){
				numbInp = pB;
			}else {

				numbInp = rnd.nextInt(pB)+1;
			}

			pileB.remove(numbInp);
		}else if(choice == 'c') {
			if(pA == 0 && pB == 0 && pC != 1 ) {
				numbInp = pC-1;
			}else if((pB == 0 && pA == 1) || (pB == 1 && pA == 0)) {
				numbInp = pC;
			}else {
				numbInp = rnd.nextInt(pC)+1;
			}

			pileC.remove(numbInp);
		}


		System.out.print("\nComputer takes "+numbInp+" from "+Character.toUpperCase(choice));


	}
	public boolean done() {
		// Is the game done?
		int pA = pileA.getSize();
		int pB = pileB.getSize();
		int pC = pileC.getSize();

		if(pA == 0 && pB == 0 && pC == 0) {
			return true;
		}else {
			return false;
		}

	}
	public void printPiles() {

		int a = pileA.getSize();
		int b = pileB.getSize();
		int c = pileC.getSize();

		System.out.print("\nA\tB\tC");
		System.out.print("\n"+a+"\t"+b+"\t"+c+"\n");


	}
}
